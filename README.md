# COMP7405 Assignment 3

This the project of the assignment 3 of comp7405


## Based on [Python Flask Skeleton](http://flask.pocoo.org). for Google App Engine forked form [appengine-flask-skeleton](https://github.com/GoogleCloudPlatform/appengine-flask-skeleton) in favor of the sample [here](https://github.com/GoogleCloudPlatform/python-docs-samples/tree/master/appengine/standard/flask/hello_world).

## Run This Project Locally
1. Install the [App Engine Python SDK](https://developers.google.com/appengine/downloads).
See the README file for directions. You'll need python 2.7 and [pip 1.4 or later](http://www.pip-installer.org/en/latest/installing.html) installed too.

2. Clone this repo with

   ```
   git clone https://github.com/houyewei/comp7405.git
   ```
3. Install dependencies in the project's lib directory.
   Note: App Engine can only import libraries from inside your project directory.

   ```
   cd comp7405
   pip install -r requirements.txt -t lib
   ```
4. Run this project locally from the command line:

   ```
   dev_appserver.py .
   ```

Visit the application [http://localhost:8080](http://localhost:8080)

See [the development server documentation](https://developers.google.com/appengine/docs/python/tools/devserver)
for options when running dev_appserver.





## Contributing changes
See [CONTRIB.md](CONTRIB.md)

## Licensing
See [LICENSE](LICENSE)

## Author
HOU Yewei ZHANG Yupeng LING Geng
Python Flask Skeleton by Logan Henriquez and Johan Euphrosine
