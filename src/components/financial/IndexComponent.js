'use strict';

import React from 'react';

require('styles/financial/Index.css');

class IndexComponent extends React.Component {
  render() {
    return (
      <div className="index-component">
        Please edit src/components/financial//IndexComponent.js to update this component!
      </div>
    );
  }
}

IndexComponent.displayName = 'FinancialIndexComponent';

// Uncomment properties you need
// IndexComponent.propTypes = {};
// IndexComponent.defaultProps = {};

export default IndexComponent;
