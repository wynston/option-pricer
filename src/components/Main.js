require('normalize.css/normalize.css');
require('styles/App.css');

import React from 'react';
import ReactDOM from 'react-dom'
import ReactHighcharts from 'react-highcharts'
import Highcharts from 'highcharts'
import axios from 'axios'
import DatePicker from 'react-datepicker';
import moment from 'moment';


import 'react-datepicker/dist/react-datepicker.css';

import { Button, FormControl, FormGroup, ControlLabel, Col, Tabs, Tab } from 'react-bootstrap';

let imageSrc = require('../images/ajax-load.gif');

class AppComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            tabIndex: 0,
            form: {
                type: -1
            },
            shouldHide: true,
            startDate: moment()
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleSelect = this.handleSelect.bind(this);
        this.handleDateChange = this.handleDateChange.bind(this);
    }
    componentDidMount() {
    }
    handleChange(event) {
        let data = this.state.form
        data[event.target.name] = event.target.value
        console.log(data)
        this.setState({form: data});
        console.log(event)
    }
    handleSelect(eventKey) {
        console.log(eventKey)
        this.setState({
            tabIndex: eventKey,
            form: {
                type: -1
            }
        });
    }
    validateEmpty(value) {
        return true;
    }
    handleDateChange(date) {
        this.setState({
          startDate: date
        });
      }

    handleSubmit(event) {
        // alert('A name was submitted: ' + this.state);
        event.preventDefault();
        let data = this.state.form
        data['tab'] = this.state.tabIndex
        this.setState({
            form: data
        }) 
        console.log(this.state.tabIndex)
        this.setState({shouldHide: false})
        let me = this;
        setTimeout(function(){
            me.setState({shouldHide: true})
        }, 15000)
        axios.post('/api', this.state.form).then(res=>{
            this.setState({shouldHide: true})
            const data = res.data
            console.log(data)
            this.setState({result: data.data})
            if(data.ci&& data.ci.length) {
                this.setState({ci: '['+data.ci[0]+', '+data.ci[1]+']'})
            }
        })
    }
  render() {
    return (
      <div className="index">
        <h1 className="title">Option Pricer</h1>
        <h5 className="title">Comp7405 Assignment 3</h5>
        <h6 className="title">By HOU Yewei, ZHANG Yupeng, LING Geng</h6>
        
        
        
          <Tabs defaultActiveKey={0} onSelect={this.handleSelect} id="controlled" animation={false}>
        <Tab eventKey={0} title="European Options" >
            
                <form className="main-form">
                <div className="row">
                    <Col sm={12}>
                        <Col sm={12}>
                            <h5>Black-Scholes Formulas for European call/put options</h5>
                        </Col>
                    </Col>
                </div>
                <FormGroup className="row">
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>S</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="S" placeholder="Spot Price" onChange={this.handleChange} value={this.state.S} />
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>r</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="r" placeholder="Risk-free Interest Rate"  onChange={this.handleChange} value={this.state.r}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>q</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="q" placeholder="Repo Rate"  onChange={this.handleChange} value={this.state.q}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>T</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                          <FormControl type="number" name="T" title="Time to Maturity (in years)" placeholder="Time to Maturity (in years)"  onChange={this.handleChange} value={this.state.T}/>
                        </Col>
                    </Col>
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>Sigma</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="sigma" placeholder="Sigma" onChange={this.handleChange} value={this.state.sigma} />
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>K</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="K" placeholder="Strike Price"  onChange={this.handleChange} value={this.state.K}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>Type</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                            <FormControl componentClass="select" name="type" placeholder="select" value={this.state.type}  onChange={this.handleChange}>
                                <option value={-1}>PUT</option>
                                <option value={1}>CALL</option>
                            </FormControl>
                        </Col>
                    </Col>
                </FormGroup>
                <FormGroup className="row">
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={3}></Col>
                        <Col componentClass={ControlLabel} sm={9}>
                        <Button type="button" bsStyle="primary" className="pull-right calculate-btn" onClick={this.handleSubmit}>Submit</Button>
                        </Col>
                    </Col>
                </FormGroup>
                <FormGroup className="row result" bsSize="large">
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={2}>
                            <ControlLabel>Option Price: </ControlLabel>
                        </Col>
                        <Col sm={5}>
                            <FormControl type="number" name="Result" placeholder="Result"  onChange={this.handleChange} value={this.state.result}/>
                        </Col>
                    </Col>
                </FormGroup>
          </form>
            
            </Tab>
                   <Tab eventKey={6} title="Volatility Calculator">
            <form className="main-form">
                <div className="row">
                    <Col sm={12}>
                        <Col sm={12}>
                            <h5>Volatility Calculator</h5>
                        </Col>
                    </Col>
                </div>
                <FormGroup className="row">
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>S</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="S" placeholder="Spot Price" onChange={this.handleChange} value={this.state.S} />
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>r</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="r" placeholder="Risk-free Interest Rate"  onChange={this.handleChange} value={this.state.r}/>
                        </Col>
        
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>q</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="q" placeholder="Repo Rate"  onChange={this.handleChange} value={this.state.q}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>T</ControlLabel>
                        </Col>
                        
                        <Col componentClass={ControlLabel} sm={2}>
                          <FormControl type="number" name="T" placeholder="Time to Maturity (in years)"  onChange={this.handleChange} value={this.state.T}/>
                        </Col>
        
                    </Col>
                    <Col sm={12}>
                        
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>K</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="K" placeholder="Strike Price"  onChange={this.handleChange} value={this.state.K}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={3}>
                            <ControlLabel>Option Premium</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={3}>
                          <FormControl type="number" name="opPrice" placeholder="Option Premium"  onChange={this.handleChange} value={this.state.opPrice}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>Type</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                            <FormControl componentClass="select" name="type" placeholder="select" value={this.state.type}  onChange={this.handleChange}>
                                <option value={-1}>PUT</option>
                                <option value={1}>CALL</option>
                            </FormControl>
                        </Col>
                    </Col>
                </FormGroup>
                <FormGroup className="row">
                    <Col sm={12}>
                        <Col sm={12}>
                            <Button type="button" bsStyle="primary" className="pull-right calculate-btn" onClick={this.handleSubmit}>Submit</Button>
                        </Col>
                    </Col>
                </FormGroup>
                <FormGroup className="row result" bsSize="large">
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={2}>
                            <ControlLabel>Sigma Price: </ControlLabel>
                        </Col>
                        <Col sm={5}>
                            <FormControl type="number" name="Result" placeholder="Result"  onChange={this.handleChange} value={this.state.result}/>
                        </Col>
                    </Col>
                </FormGroup>
          </form>
        </Tab> 
        <Tab eventKey={2} title="Geometric Asian Options">
            <form className="main-form">
                <div className="row">
                    <Col sm={12}>
                        <Col sm={12}>
                            <h5>Geometric Asian Options</h5>
                        </Col>
                    </Col>
                </div>
                <FormGroup className="row">
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>S</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="S" placeholder="Spot Price" onChange={this.handleChange} value={this.state.S} />
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>r</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="r" placeholder="Risk-free Interest Rate"  onChange={this.handleChange} value={this.state.r}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>T</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                          <FormControl type="number" name="T" placeholder="Time to Maturity (in years)"  onChange={this.handleChange} value={this.state.T}/>
                        </Col>
                    </Col>
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>Sigma</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="sigma" placeholder="Sigma" onChange={this.handleChange} value={this.state.sigma} />
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>K</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="K" placeholder="Strike Price"  onChange={this.handleChange} value={this.state.K}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>N</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                          <FormControl type="number" name="N" placeholder="N"  onChange={this.handleChange} value={this.state.N}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>Type</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                            <FormControl componentClass="select" name="type" placeholder="select" value={this.state.type}  onChange={this.handleChange}>
                                <option value={-1}>PUT</option>
                                <option value={1}>CALL</option>
                            </FormControl>
                        </Col>
                    </Col>
                </FormGroup>
                <FormGroup className="row">
                    <Col sm={12}>
                        <Col sm={12}>
                            <Button type="button" bsStyle="primary" className="pull-right calculate-btn" onClick={this.handleSubmit}>Submit</Button>
                        </Col>
                    </Col>
                </FormGroup>
                <FormGroup className="row result" bsSize="large">
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={2}>
                            <ControlLabel>Option Price: </ControlLabel>
                        </Col>
                        <Col sm={5}>
                            <FormControl type="number" name="Result" placeholder="Result"  onChange={this.handleChange} value={this.state.result}/>
                        </Col>
                    </Col>
                </FormGroup>
          </form>
        </Tab>
        <Tab eventKey={3} title="Arithmetic Asian Options">
            <form className="main-form">
                <div className="row">
                    <Col sm={12}>
                        <Col sm={12}>
                            <h5>Arithmetic Asian Options</h5>
                        </Col>
                    </Col>
                </div>
                <FormGroup className="row">
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>S</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="S" placeholder="Spot Price" onChange={this.handleChange} value={this.state.S} />
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>r</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="r" placeholder="Risk-free Interest Rate"  onChange={this.handleChange} value={this.state.r}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>T</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                          <FormControl type="number" name="T" placeholder="Time to Maturity (in years)"  onChange={this.handleChange} value={this.state.T}/>
                        </Col>
                    </Col>
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>Sigma</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="sigma" placeholder="Sigma" onChange={this.handleChange} value={this.state.sigma} />
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>K</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="K" placeholder="Strike Price"  onChange={this.handleChange} value={this.state.K}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>N</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                          <FormControl type="number" name="N" placeholder="N"  onChange={this.handleChange} value={this.state.N}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>Type</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                            <FormControl componentClass="select" name="type" placeholder="select" value={this.state.type}  onChange={this.handleChange}>
                                <option value={-1}>PUT</option>
                                <option value={1}>CALL</option>
                            </FormControl>
                        </Col>
                    </Col>
                </FormGroup>
                <FormGroup className="row">
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>M</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                            <FormControl type="number" name="M" placeholder="Strike Price"  onChange={this.handleChange} value={this.state.M}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>cvm</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                            <FormControl type="number" name="cvm" placeholder="cvm"  onChange={this.handleChange} value={this.state.cvm}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={6}>
                            <Button type="button" bsStyle="primary" className="pull-right calculate-btn" onClick={this.handleSubmit}>Submit</Button>
                        </Col>
                    </Col>
                </FormGroup>
                <FormGroup className="row result" bsSize="large">
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={2}>
                            <ControlLabel>Option Price: </ControlLabel>
                        </Col>
                        <Col sm={3}>
                            <FormControl type="number" name="Result" placeholder="Result"  onChange={this.handleChange} value={this.state.result}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={3}>
                            <ControlLabel>Confidence Interval: </ControlLabel>
                        </Col>
                        <Col sm={3}>
                            <FormControl type="text" name="ci" placeholder="Confidence Interval"  onChange={this.handleChange} value={this.state.ci}/>
                        </Col>
                    </Col>
                </FormGroup>
          </form>
        </Tab>
        <Tab eventKey={4} title="Geometric Basket Options">
            <form className="main-form">
                <div className="row">
                    <Col sm={12}>
                        <Col sm={12}>
                            <h5>Geometric Basket Options</h5>
                        </Col>
                    </Col>
                </div>
                <FormGroup className="row">
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>S1</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="S1" placeholder="Spot Price of Asset 1" onChange={this.handleChange} value={this.state.S1} />
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>Sigma1</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="sigma1" placeholder="Sigma1" onChange={this.handleChange} value={this.state.sigma1} />
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>r</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="r" placeholder="Risk-free Interest Rate"  onChange={this.handleChange} value={this.state.r}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>T</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                          <FormControl type="number" name="T" placeholder="Time to Maturity (in years)"  onChange={this.handleChange} value={this.state.T}/>
                        </Col>
                    </Col>
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>S2</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="S2" placeholder="Spot Price of Asset 2" onChange={this.handleChange} value={this.state.S2} />
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>Sigma2</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="sigma2" placeholder="Sigma2" onChange={this.handleChange} value={this.state.sigma2} />
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>K</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="K" placeholder="Strike Price"  onChange={this.handleChange} value={this.state.K}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>ρ</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="rho" placeholder="correlation ρ"  onChange={this.handleChange} value={this.state.rho}/>
                        </Col>
                    </Col>
                </FormGroup>
                <FormGroup className="row">
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>Type</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                            <FormControl componentClass="select" name="type" placeholder="select" value={this.state.type}  onChange={this.handleChange}>
                                <option value={-1}>PUT</option>
                                <option value={1}>CALL</option>
                            </FormControl>
                        </Col>
                        <Col componentClass={ControlLabel} sm={9}>
                            <Button type="button" bsStyle="primary" className="pull-right calculate-btn" onClick={this.handleSubmit}>Submit</Button>
                        </Col>
                    </Col>
                </FormGroup>
                <FormGroup className="row result" bsSize="large">
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={2}>
                            <ControlLabel>Option Price: </ControlLabel>
                        </Col>
                        <Col sm={5}>
                            <FormControl type="number" name="Result" placeholder="Result"  onChange={this.handleChange} value={this.state.result}/>
                        </Col>
                    </Col>
                </FormGroup>
          </form>
        </Tab>
        <Tab eventKey={5} title="Arithmetic Mean Basket Options">
            <form className="main-form">
                <div className="row">
                    <Col sm={12}>
                        <Col sm={12}>
                            <h5>Arithmetic Mean Basket Options</h5>
                        </Col>
                    </Col>
                </div>
                <FormGroup className="row">
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>S1</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="S1" placeholder="Spot Price of Asset 1" onChange={this.handleChange} value={this.state.S1} />
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>Sigma1</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="sigma1" placeholder="Sigma1" onChange={this.handleChange} value={this.state.sigma1} />
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>r</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="r" placeholder="Risk-free Interest Rate"  onChange={this.handleChange} value={this.state.r}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>T</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                          <FormControl type="number" name="T" placeholder="Time to Maturity (in years)"  onChange={this.handleChange} value={this.state.T}/>
                        </Col>
                    </Col>
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>S2</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="S2" placeholder="Spot Price of Asset 2" onChange={this.handleChange} value={this.state.S2} />
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>Sigma2</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="sigma2" placeholder="Sigma2" onChange={this.handleChange} value={this.state.sigma2} />
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>K</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="K" placeholder="Strike Price"  onChange={this.handleChange} value={this.state.K}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>ρ</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="rho" placeholder="correlation ρ"  onChange={this.handleChange} value={this.state.rho}/>
                        </Col>
                    </Col>
                </FormGroup>
                <FormGroup className="row">
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>Type</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                            <FormControl componentClass="select" name="type" placeholder="select" value={this.state.type}  onChange={this.handleChange}>
                                <option value={-1}>PUT</option>
                                <option value={1}>CALL</option>
                            </FormControl>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>M</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                            <FormControl type="number" name="M" placeholder="Strike Price"  onChange={this.handleChange} value={this.state.M}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>cvm</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                            <FormControl type="number" name="cvm" placeholder="cvm"  onChange={this.handleChange} value={this.state.cvm}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={3}>
                            <Button type="button" bsStyle="primary" className="pull-right calculate-btn" onClick={this.handleSubmit}>Submit</Button>
                        </Col>
                    </Col>
                </FormGroup>
                <FormGroup className="row result" bsSize="large">
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={2}>
                            <ControlLabel>Option Price: </ControlLabel>
                        </Col>
                        <Col sm={3}>
                            <FormControl type="number" name="Result" placeholder="Result"  onChange={this.handleChange} value={this.state.result}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={3}>
                            <ControlLabel>Confidence Interval: </ControlLabel>
                        </Col>
                        <Col sm={3}>
                            <FormControl type="text" name="ci" placeholder="Confidence Interval"  onChange={this.handleChange} value={this.state.ci}/>
                        </Col>
                    </Col>
                </FormGroup>
          </form>
        </Tab>

        <Tab eventKey={1} title="American Options" >
                <form className="main-form">
                <div className="row">
                    <Col sm={12}>
                        <Col sm={12}>
                            <h5>American Options</h5>
                        </Col>
                    </Col>
                </div>
                <FormGroup className="row">
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>S</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="S" placeholder="Spot Price" onChange={this.handleChange} value={this.state.S} />
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>r</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="r" placeholder="Risk-free Interest Rate"  onChange={this.handleChange} value={this.state.r}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>T</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                          <FormControl type="number" name="T" placeholder="Time to Maturity (in years)"  onChange={this.handleChange} value={this.state.T}/>
                        </Col>
                    </Col>
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>Sigma</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="sigma" placeholder="Sigma" onChange={this.handleChange} value={this.state.sigma} />
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>K</ControlLabel>
                        </Col>
                        <Col sm={2}>
                            <FormControl type="number" name="K" placeholder="Strike Price"  onChange={this.handleChange} value={this.state.K}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>N</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                          <FormControl type="number" name="N" placeholder="N"  onChange={this.handleChange} value={this.state.N}/>
                        </Col>
                        <Col componentClass={ControlLabel} sm={1}>
                            <ControlLabel>Type</ControlLabel>
                        </Col>
                        <Col componentClass={ControlLabel} sm={2}>
                            <FormControl componentClass="select" name="type" placeholder="select" value={this.state.type}  onChange={this.handleChange}>
                                <option value={-1}>PUT</option>
                                <option value={1}>CALL</option>
                            </FormControl>
                        </Col>
                    </Col>
                </FormGroup>
                <FormGroup className="row">
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={12}>
                            <Button type="button" bsStyle="primary" className="pull-right calculate-btn" onClick={this.handleSubmit}>Submit</Button>
                        </Col>
                    </Col>
                </FormGroup>
                <FormGroup className="row result" bsSize="large">
                    <Col sm={12}>
                        <Col componentClass={ControlLabel} sm={2}>
                            <ControlLabel>Option Price: </ControlLabel>
                        </Col>
                        <Col sm={5}>
                            <FormControl type="number" name="Result" placeholder="Result"  onChange={this.handleChange} value={this.state.result}/>
                        </Col>
                    </Col>
                </FormGroup>
          </form>
        </Tab> 
        </Tabs>
        <div className={this.state.shouldHide ? 'hidden loading-wrap' : 'loading-wrap '}>
            <div className="loading"></div>
            <img className="loading-img" src={imageSrc} />
            <h7>Processing ... Please wait</h7>
        </div>
      </div>
    
    );
  }
}






AppComponent.defaultProps = {
};

export default AppComponent;
