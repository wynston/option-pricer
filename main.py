"""`main` is the top level module for your Flask application."""

# Import the Flask Framework
from flask import Flask, request, jsonify
import json, time, random
from datetime import tzinfo, timedelta, datetime
from flask_cors import CORS, cross_origin
import util

app = Flask(__name__, static_url_path="", static_folder="dist")
CORS(app)

app.config['DEBUG'] = True

# Note: We don't need to call run() since our application is embedded within
# the App Engine WSGI application server.

@app.route('/')
def root():
    return app.send_static_file('index.html')

@app.route('/api', methods=['GET'])
def api_get():
    """Return a friendly HTTP greeting."""
    return 'Hello World!'

@app.route('/api', methods=['POST'])
def api_post():
    """The parameters are transfered in JSON format"""
    content = request.get_json(silent=True)
    print content
    ci = 0
    S = float(content.get(u'S', 0))
    S1 = float(content.get(u'S1', 0))
    S2 = float(content.get(u'S2', 0))
    r = float(content.get(u'r', 0))
    q = float(content.get(u'q', 0))
    T = float(content.get(u'T', 0))
    K = float(content.get(u'K', 0))
    sigma = float(content.get(u'sigma', 0))
    sigma1 = float(content.get(u'sigma1', 0))
    sigma2 = float(content.get(u'sigma2', 0))
    rho = float(content.get(u'rho', 0))
    N = int(content.get(u'N', 0))
    M = int(content.get(u'M', 0))
    cvm = int(content.get(u'cvm', 0))
    ot = int(content.get(u'type', 0))
    tab = int(content.get(u'tab', 0))
    opPrice = float(content.get(u'opPrice', 1))
    if tab == 0:
        value = util.bsFormulas(S, K, T, sigma, r, q, ot)
    elif tab == 1:
        value = util.binomialTreeMethod(S, K, T, sigma, r, N, ot)
    elif tab == 2:
        value = util.cfGAsianOption(S, K, T, sigma, r, N, ot)
    elif tab == 3:
        value, ci = util.mcCVAsianOption(S, K, T, sigma, r, N, M, cvm, ot)
    elif tab == 4:
        value = util.cfGBasketOption(S1, S2, K, T, sigma1, sigma2, r, rho, ot)
    elif tab == 5:
        value, ci = util.mcCVBasketoption(S1, S2, K, T, sigma1, sigma2, r, M, cvm, rho, ot)
    elif tab == 6:
        value = util.volCalculation(S, K, T, r, q, opPrice, ot)
    if ci:
        res = {'success':True, 'data': value, 'ci': ci}
    else:
        res = {'success':True, 'data': value}
    return json.dumps(res)

# demo function of the flask api
@app.route('/demo', methods=['GET', 'POST'])
def demo():
    arr = []
    day = timedelta(days=1)
    timetik = datetime.now()
    for i in range(1, 100):
        timetik += day
        timestamp = int(time.mktime(timetik.timetuple()))*1000
        value = random.random()
        arr.append([timestamp, value])
    return json.dumps(arr)


@app.errorhandler(404)
def page_not_found(e):
    """Return a custom 404 error."""
    return 'Sorry, Nothing at this URL.', 404


@app.errorhandler(500)
def application_error(e):
    """Return a custom 500 error."""
    return 'Sorry, unexpected error: {}'.format(e), 500

if __name__ == "__main__":
    app.run(host="0.0.0.0")
