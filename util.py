
from __future__ import division
import math
from scipy.stats import norm
import numpy as np



# task 1: Implement Black-Scholes Formulas for European call/put options
def bsFormulas(S, K, deltaT, sigma, r, q, ot):
	print ot
	sigmat = sigma * math.sqrt(deltaT)
	d1 = ((math.log(1.0 * S / K) + (r - q) * (deltaT)) / sigmat) + 0.5 * sigmat
	d2 = d1 - sigmat
	if ot == 1:
		optionPrice = S * math.exp(-q * deltaT) * norm.cdf(d1) - K * math.exp(-r * deltaT) * norm.cdf(d2)
	elif ot == -1:
		optionPrice = K * math.exp(-r * deltaT) * norm.cdf(-1 * d2) - S * math.exp(-q * deltaT) * norm.cdf(-1 * d1)
	print optionPrice
	return round(optionPrice, 4)


# task 2: Implied volatility calculations
# auxiliary function
def price(S, K, deltaT, sigma, r, q):
	sigmat = sigma * math.sqrt(deltaT)
	d1 = ((math.log(S / K) + (r - q) * deltaT) / sigmat) + 0.5 * sigmat
	d2 = d1 - sigmat

	P = K * math.exp(-r * deltaT) * norm.cdf(-1 * d2) - S * math.exp(-q * deltaT) * norm.cdf(-1 * d1)
	C = S * math.exp(-q * deltaT) * norm.cdf(d1) - K * math.exp(-r * deltaT) * norm.cdf(d2)
	dP = S * math.exp(-q * deltaT) * math.sqrt(deltaT) * norm.pdf(d1)
	dC = dP
	return P, C, dP, dC
# Implied volatility calculations, Newton's method
def volCalculation(S, K, deltaT, r, q, opPrice, ot):
	sigmahat = math.sqrt(2 * abs((math.log(S / K) + (r - q) * deltaT) / deltaT))
	sigma = sigmahat
	eps = 1e-8
	sigmadiff = 1
	n = 1
	nmax = 100
	if ot == 1:
		while sigmadiff >= eps and n < nmax:
			P, C, dP, dC = price(S, K, deltaT, sigma, r, q)
			if dC == 0:
				break
			else:
				increment = (C - opPrice) / dC
				sigma -= increment
				n += 1
				sigmadiff = abs(increment)

	elif ot == -1:
		while sigmadiff >= eps and n < nmax:
			P, C, dP, dC = price(S, K, deltaT, sigma, r, q)
			if dP == 0:
				break
			else:
				increment = (P - opPrice) / dP
				sigma -= increment
				n += 1
				sigmadiff = abs(increment)

	return round(sigma, 4)


# task 3: closed-form formulas for geometric Asian call/put option
def cfGAsianOption(S, K, T, sigma, r, N, ot):
	sigsqT = sigma**2 * T * (N + 1) * (2 * N + 1) / (6 * N * N)
	muT = 0.5 * sigsqT + (r - 0.5 * sigma**2) * T * (N + 1) / (2 * N)

	d1 = (math.log(S / K) + (muT + 0.5 * sigsqT)) / (math.sqrt(sigsqT))
	d2 = d1 - math.sqrt(sigsqT)

	if ot == 1:
		N1, N2 = norm.cdf(d1), norm.cdf(d2)
		geoAO = math.exp(-r * T) * (S * math.exp(muT) * N1 - K * N2)
	elif ot == -1:
		N1, N2 = norm.cdf(-d1), norm.cdf(-d2)
		geoAO = math.exp(-r * T) * (-S * math.exp(muT) * N1 + K * N2)
	
	return round(geoAO, 4)

def cfGBasketOption(S1, S2, K, T, sigma1, sigma2, r, rho, ot):
	Sgt = math.sqrt(S1 * S2)
	sigmaBgsqT = 0.25 * T * (sigma1**2 + 2 * sigma1 * sigma2 * rho + sigma2**2) 
	muBgT = r * T - 0.25 * T * (sigma1**2 + sigma2**2) + 0.5 * sigmaBgsqT

	d1 = (math.log(Sgt / K) + (muBgT + 0.5 * sigmaBgsqT)) / (math.sqrt(sigmaBgsqT))
	d2 = d1 - math.sqrt(sigmaBgsqT)

	if ot == 1:
		N1, N2 = norm.cdf(d1), norm.cdf(d2)
		geoBO = math.exp(-r * T) * (Sgt * math.exp(muBgT) * N1 - K * N2)
	elif ot == -1:
		N1, N2 = norm.cdf(-d1), norm.cdf(-d2)
		geoBO = math.exp(-r * T) * (-Sgt * math.exp(muBgT) * N1 + K * N2)

	return round(geoBO, 4)

# task 4: Implement the MC method with control variate technique for arithmetic Asian options
def mcCVAsianOption(S, K, T, sigma, r, N, M, cvm, ot):
	# sigsqT = sigma**2 * T * (N + 1) * (2 * N + 1) / (6 * N * N)
	# muT = 0.5 * sigsqT + (r - 0.5 * sigma**2) * T * (N + 1) / (2 * N)

	dT = 1.0 * T / N
	drift = np.exp((r - 0.5 * sigma**2) * dT)
	Spath = np.zeros((M, N), dtype = float)

	for i in range(M):
		deltaZ = np.random.standard_normal()
		growthFactor = drift * np.exp(sigma * math.sqrt(dT) * deltaZ)
		Spath[i][0] = S * growthFactor
		for j in range(1, N):
			deltaZ = np.random.standard_normal()
			growthFactor = drift * np.exp(sigma * math.sqrt(dT) * deltaZ)
			Spath[i][j] = Spath[i][j-1] * growthFactor

	# Arithmetic mean
	SpanthArithMean = Spath.mean(1)

	# option type
	geoAO = math.exp(-r * T) * np.maximum(ot * (SpanthArithMean - K), 0)
	
	if cvm == 0:
		geoAOMean = geoAO.mean()
		geoAOStd = geoAO.std()

		opPrice = round(geoAOMean, 4)
		lNum = round(geoAOMean - 1.96 * geoAOStd, 4)
		rNum = round(geoAOMean + 1.96 * geoAOStd, 4)
		CI = [lNum, rNum]

	else:
		geoCV = cfGAsianOption(S, K, T, sigma, r, N, ot)
		SpanthGMean = Spath.prod(1)**(1/N)

		geo = math.exp(-r * T) * np.maximum(ot * (SpanthGMean - K), 0)

		# Control Variate
		covXY = (geoAO*geo).mean() - geoAO.mean() * geo.mean()
		theta = covXY / (geo.var())

		geoError = geoAO + theta * (geoCV - geo)
		geoErrorMean = geoError.mean()
		geoErrorStd = geoError.std()

		# control variate version
		opPrice = round(geoErrorMean, 4)
		lNum = round(geoErrorMean - 1.96 * geoErrorStd, 4)
		rNum = round(geoErrorMean + 1.96 * geoErrorStd, 4)
		CI = [lNum, rNum]

	return opPrice, CI

# task 5: Implement the MC method with control variate technique for arithmetic Basket options
def mcCVBasketoption(S1, S2, K, T, sigma1, sigma2, r, M, cvm, rho, ot):

	drift1 = np.exp((r - 0.5 * sigma1**2) * T)
	drift2 = np.exp((r - 0.5 * sigma2**2) * T)

	norm = np.random.normal(size = (M, 1))

	norm1 = np.random.normal(size = (M, 1))
	norm2 = norm1 * rho + norm * math.sqrt(1 - rho**2)

	temp1 = np.exp(norm1 * math.sqrt(T) * sigma1) *drift1 * S1
	temp2 = np.exp(norm2 * math.sqrt(T) * sigma2) *drift2 * S2
	tempMean = (temp1 + temp2) / 2

	# option type
	ariBO = math.exp(-r * T) * np.maximum(ot * (tempMean - K), 0)
	
	if cvm == 0:
		ariBOMean = ariBO.mean()
		ariBOStd = ariBO.std()

		opPrice = round(ariBOMean, 4)
		lNum = round(ariBOMean - 1.96 * ariBOStd, 4)
		rNum = round(ariBOMean + 1.96 * ariBOStd, 4)
		CI = [lNum, rNum]

	else:
		ariCV = cfGBasketOption(S1, S2, K, T, sigma1, sigma2, r, rho, ot)
		ariGBO = np.sqrt(temp1 * temp2)

		ari = math.exp(-r * T) * np.maximum(ot * (ariGBO - K), 0)

		# Control Variate
		covXY = (ariBO*ari).mean() - ariBO.mean() * ari.mean()
		theta = covXY / (ari.var())

		ariError = ariBO + theta * (ariCV - ari)
		ariErrorMean = ariError.mean()
		ariErrorStd = ariError.std()

		# control variate version
		opPrice = round(ariErrorMean, 4)
		lNum = round(ariErrorMean - 1.96 * ariErrorStd, 4)
		rNum = round(ariErrorMean + 1.96 * ariErrorStd, 4)
		CI = [lNum, rNum]

	return opPrice, CI


# task 6: Implement the Binomial Tree method for American call/put options
def binomialTreeMethod(S, K, T, sigma, r, N, ot):
	deltaT = float(T) / N
	DF = math.exp(-r * deltaT)
	u = math.exp(sigma * math.sqrt(deltaT))
	d = 1 / u
	# print deltaT, DF, u, d
	p = 1.0 * (math.exp(r * deltaT) - d) / (float(u - d))
	priceList = list()
	for i in range(1, N+2):
		priceList.append(max(ot * (S * u**(N - i + 1) * d**(i - 1) - K), 0))
	# print priceList

	for j in range(N, 0, -1):
		for k in range(0, j):
			# priceList[k] = (p * priceList[k+1] + (1-p) * priceList[k]) / DF
			priceList[k] = max(ot*(S*u**(j-k-1)*d**(k)-K), DF*(priceList[k]*p + priceList[k+1]*(1-p)))
			
	return round(priceList[0], 4)

r, T, S = 0.05, 3, 100
print bsFormulas(S, 100, T, 0.3, r, 0, -1)

'''
r, T, S = 0.05, 3, 100
print "----------*European Options*----------"
print "------Put------"
print bsFormulas(S, 100, T, 0.3, r, 0, -1)
print "------Call------"
print bsFormulas(S, 100, T, 0.3, r, 0, 1)
print ""

print "----------*Implied Volatility*-----------"
print volCalculation(50, 50, 0.25, 0.05, 0, 3.2915, 1)
print ""

print "----------*Geometric Asian Options*----------"
print "------Put------"
print cfGAsianOption(S, 100, T, 0.3, r, 50, -1)
print cfGAsianOption(S, 100, T, 0.3, r, 100, -1)
print cfGAsianOption(S, 100, T, 0.4, r, 50, -1)
print "------Call------"
print cfGAsianOption(S, 100, T, 0.3, r, 50, 1)
print cfGAsianOption(S, 100, T, 0.3, r, 100, 1)
print cfGAsianOption(S, 100, T, 0.4, r, 50, 1)
print ""

print "----------*Arthmetic Asian Options No CV*----------"
print "------Put------"
print mcCVAsianOption(S, 100, T, 0.3, r, 50, 100000, 0, -1)
print mcCVAsianOption(S, 100, T, 0.3, r, 100, 30, 0, -1)
print mcCVAsianOption(S, 100, T, 0.3, r, 50, 30, 0, -1)
print "------Call------"
print mcCVAsianOption(S, 100, T, 0.3, r, 50, 100000, 0, 1)
print mcCVAsianOption(S, 100, T, 0.3, r, 100, 30, 0, 1)
print mcCVAsianOption(S, 100, T, 0.3, r, 50, 30, 0, 1)
print ""

print "----------*Arthmetic Asian Options with CV*----------"
print "------Put------"
print mcCVAsianOption(S, 100, T, 0.3, r, 50, 30, 1, -1)
print mcCVAsianOption(S, 100, T, 0.3, r, 100, 30, 1, -1)
print mcCVAsianOption(S, 100, T, 0.4, r, 50, 30, 1, -1)
print "------Call------"
print mcCVAsianOption(S, 100, T, 0.3, r, 50, 30, 1, 1)
print mcCVAsianOption(S, 100, T, 0.3, r, 100, 30, 1, 1)
print mcCVAsianOption(S, 100, T, 0.4, r, 50, 30, 1, 1)
print ""

print "----------*Geometric Basket Options*----------"
print "------Put------"
print cfGBasketOption(100, 100, 100, T, 0.3, 0.3, r, 0.5, -1)
print cfGBasketOption(100, 100, 100, T, 0.3, 0.3, r, 0.9, -1)
print cfGBasketOption(100, 100, 100, T, 0.1, 0.3, r, 0.5, -1)
print cfGBasketOption(100, 100, 80, T, 0.3, 0.3, r, 0.5, -1)
print cfGBasketOption(100, 100, 120, T, 0.3, 0.3, r, 0.5, -1)
print cfGBasketOption(100, 100, 100, T, 0.5, 0.5, r, 0.5, -1)
print "------Call------"
print cfGBasketOption(100, 100, 100, T, 0.3, 0.3, r, 0.5, 1)
print cfGBasketOption(100, 100, 100, T, 0.3, 0.3, r, 0.9, 1)
print cfGBasketOption(100, 100, 100, T, 0.1, 0.3, r, 0.5, 1)
print cfGBasketOption(100, 100, 80, T, 0.3, 0.3, r, 0.5, 1)
print cfGBasketOption(100, 100, 120, T, 0.3, 0.3, r, 0.5, 1)
print cfGBasketOption(100, 100, 100, T, 0.5, 0.5, r, 0.5, 1)
print ""

print "----------*Arthmetic Basket Options NO CV*----------"
print "------Put------"
print mcCVBasketoption(100, 100, 100, T, 0.3, 0.3, r, 100000, 0, 0.5, -1)
print mcCVBasketoption(100, 100, 100, T, 0.3, 0.3, r, 100, 0, 0.9, -1)
print mcCVBasketoption(100, 100, 100, T, 0.1, 0.3, r, 100, 0, 0.5, -1)
print mcCVBasketoption(100, 100, 80, T, 0.3, 0.3, r, 100, 0, 0.5, -1)
print mcCVBasketoption(100, 100, 120, T, 0.3, 0.3, r, 100, 0, 0.5, -1)
print mcCVBasketoption(100, 100, 100, T, 0.5, 0.5, r, 100, 0, 0.5, -1)
print "------Call------"
print mcCVBasketoption(100, 100, 100, T, 0.3, 0.3, r, 100000, 0, 0.5, 1)
print mcCVBasketoption(100, 100, 100, T, 0.3, 0.3, r, 100, 0, 0.9, 1)
print mcCVBasketoption(100, 100, 100, T, 0.1, 0.3, r, 100, 0, 0.5, 1)
print mcCVBasketoption(100, 100, 80, T, 0.3, 0.3, r, 100, 0, 0.5, 1)
print mcCVBasketoption(100, 100, 120, T, 0.3, 0.3, r, 100, 0, 0.5, 1)
print mcCVBasketoption(100, 100, 100, T, 0.5, 0.5, r, 100, 0, 0.5, 1) 
print ""

print "----------*Arthmetic Basket Options with CV*----------"
print "------Put------"
print mcCVBasketoption(100, 100, 100, T, 0.3, 0.3, r, 100000, 1, 0.5, -1)
print mcCVBasketoption(100, 100, 100, T, 0.3, 0.3, r, 100, 1, 0.9, -1)
print mcCVBasketoption(100, 100, 100, T, 0.1, 0.3, r, 100, 1, 0.5, -1)
print mcCVBasketoption(100, 100, 80, T, 0.3, 0.3, r, 100, 1, 0.5, -1)
print mcCVBasketoption(100, 100, 120, T, 0.3, 0.3, r, 100, 1, 0.5, -1)
print mcCVBasketoption(100, 100, 100, T, 0.5, 0.5, r, 100, 1, 0.5, -1)
print "------Call------"
print mcCVBasketoption(100, 100, 100, T, 0.3, 0.3, r, 100000, 1, 0.5, 1)
print mcCVBasketoption(100, 100, 100, T, 0.3, 0.3, r, 100, 1, 0.9, 1)
print mcCVBasketoption(100, 100, 100, T, 0.1, 0.3, r, 100, 1, 0.5, 1)
print mcCVBasketoption(100, 100, 80, T, 0.3, 0.3, r, 100, 1, 0.5, 1)
print mcCVBasketoption(100, 100, 120, T, 0.3, 0.3, r, 100, 1, 0.5, 1)
print mcCVBasketoption(100, 100, 100, T, 0.5, 0.5, r, 100, 1, 0.5, 1) 
print ""

print "----------*American Options*----------"
print "------Put------"
print binomialTreeMethod(S, 100, T, 0.3, r, 100, -1)
print "------Call------"
print binomialTreeMethod(S, 100, T, 0.3, r, 100, 1)
print ""

'''











